# wg-federation

## Description
OCI code to build the [wg-federation](https://gitlab.com/anuvumtl/python/wg-federation) container.

## Tags
We push a `latest` tag on this repository, to run an older version please checkout the different tags.

## Usage

## Labels
We set labels on our images with additional information on the image.
We follow the guidelines defined at http://label-schema.org/.
Visit their website for more information about those labels.

## Contributions
Contributions are welcome.
