ARG IMAGE="docker.io/library/alpine"
ARG IMAGE_TAG="3.17"

FROM ${IMAGE}:${IMAGE_TAG}

ARG IMAGE
ARG IMAGE_TAG
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_DESCRIPTION
ARG VCS_URL
ARG VCS_TITLE
ARG VCS_NAMESPACE
ARG LICENSE
ARG VERSION

ENV PYTHON_VERSION=3.10.10-r0 \
    WG_FEDERATION_VERSION=0.8.0

VOLUME /data

COPY ./resources /resources

RUN /resources/build && rm -rf /resources

WORKDIR /data

ENTRYPOINT ["wg-federation"]

LABEL "org.opencontainers.image.created"=${BUILD_DATE} \
      "org.opencontainers.image.authors"="${VCS_NAMESPACE} - https://gitlab.com/anuvumtl/" \
      "org.opencontainers.image.url"="${VCS_URL}" \
      "org.opencontainers.image.documentation"="${VCS_URL}" \
      "org.opencontainers.image.source"="${VCS_URL}" \
      "org.opencontainers.image.version"="${VERSION}" \
      "org.opencontainers.image.vendor"="${VCS_NAMESPACE}" \
      "org.opencontainers.image.licenses"="${LICENSE}" \
      "org.opencontainers.image.title"="${VCS_TITLE}" \
      "org.opencontainers.image.description"="${VCS_DESCRIPTION}" \
      "org.opencontainers.image.base.name"="${IMAGE}:${IMAGE_TAG}" \

      "org.opencontainers.applications.wg-federation.version"=$WG_FEDERATION_VERSION \
      "org.opencontainers.dependencies.python"=$PYTHON_VERSION
