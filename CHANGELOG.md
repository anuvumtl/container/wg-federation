0.2.0
=====

- refactor: updates labels to opencontainers.
- refactor: uses `Containerfile` instead of `Dockerfile`.
- doc: updates `README.md` to reflect repository change.
- maintenance: updates `.pre-commit` dependencies.
- test: adds `.gitlab-ci` basic pipeline

0.1.0
=====

- feat: first working version

0.0.0
=====

- tech: initial version
